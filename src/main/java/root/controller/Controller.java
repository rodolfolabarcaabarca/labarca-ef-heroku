/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import root.model.dao.PalabrasDAO;
import root.persistence.entities.Palabras;
import root.services.PalabraRestLocal;

/**
 *
 * @author rlabarca
 */
@WebServlet(name = "Controller", urlPatterns = {"/controller"})
public class Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* Formato Variable Fecha */
            Date fechaActual = new Date();
            
            /** Captura de Accion a Relizar **/ 
            String palabra = request.getParameter("palabra");
            String origPalabra = palabra; 
            palabra = PalabraRestLocal.eliminarAcentos(palabra);
            palabra = palabra.toLowerCase();
            
            /* Definicion de Cliente Rest API LOCAL */
            String baseURL = "https://rlabarca-ef.herokuapp.com/api/palabra/";            
            OkHttpClient clienteWEB = new OkHttpClient();
            Request req = new Request.Builder().url(baseURL + palabra).get().addHeader("Content-Type","text/json;Charset=UTF-8").build();
            com.squareup.okhttp.Response respuesta = clienteWEB.newCall(req).execute();
            String strResp = respuesta.body().string(); 
            
            /* Validacion si es que Palabra no Existe, En caso contrario lo guarda en la BBDD */
            if (strResp.charAt(0) != '*') {
                
                PalabrasDAO dao = new PalabrasDAO(); 
                
                Palabras ingPalabra = new Palabras();
                ingPalabra.setPalId(origPalabra);
                ingPalabra.setPalDesc(strResp);
                ingPalabra.setPalFecha(fechaActual);
                
                /* Valida si Existe, si Existe Actualiza la Fecha de Consulta, Si no la Crea*/
                Palabras checkExiste = dao.findPalabras(origPalabra); 
                if (checkExiste != null) {
                    dao.edit(ingPalabra);
                } else {
                    dao.create(ingPalabra);
                }
            }
            request.setAttribute("mensajeSalida", strResp);
            request.getRequestDispatcher("respuesta.jsp").forward(request, response);
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
