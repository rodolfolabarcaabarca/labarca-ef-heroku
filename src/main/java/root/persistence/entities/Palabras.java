/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rlabarca
 */
@Entity
@Table(name = "palabras")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Palabras.findAll", query = "SELECT p FROM Palabras p"),
    @NamedQuery(name = "Palabras.findByPalId", query = "SELECT p FROM Palabras p WHERE p.palId = :palId"),
    @NamedQuery(name = "Palabras.findByPalFecha", query = "SELECT p FROM Palabras p WHERE p.palFecha = :palFecha"),
    @NamedQuery(name = "Palabras.findByPalDesc", query = "SELECT p FROM Palabras p WHERE p.palDesc = :palDesc")})
public class Palabras implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "pal_id")
    private String palId;
    @Column(name = "pal_fecha")
    @Temporal(TemporalType.DATE)
    private Date palFecha;
    @Size(max = 2147483647)
    @Column(name = "pal_desc")
    private String palDesc;

    public Palabras() {
    }

    public Palabras(String palId) {
        this.palId = palId;
    }

    public String getPalId() {
        return palId;
    }

    public void setPalId(String palId) {
        this.palId = palId;
    }

    public Date getPalFecha() {
        return palFecha;
    }

    public void setPalFecha(Date palFecha) {
        this.palFecha = palFecha;
    }

    public String getPalDesc() {
        return palDesc;
    }

    public void setPalDesc(String palDesc) {
        this.palDesc = palDesc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (palId != null ? palId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Palabras)) {
            return false;
        }
        Palabras other = (Palabras) object;
        if ((this.palId == null && other.palId != null) || (this.palId != null && !this.palId.equals(other.palId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.persistence.entities.Palabras[ palId=" + palId + " ]";
    }
    
}