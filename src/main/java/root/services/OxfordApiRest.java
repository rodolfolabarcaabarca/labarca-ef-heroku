/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject; 
import org.json.JSONArray;

/**
 *
 * @author rlabarca
 */
@Path("/ox")
public class OxfordApiRest {
    @GET 
    @Path("/{pBuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public JSONObject buscarOX(@PathParam("pBuscar") String pBuscar) throws IOException{
        /** Formato Fecha para LOGS**/
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date(System.currentTimeMillis()); 
        /** Traer JSON de OXFORD DICTIONARY en FORMATO JSON **/        
        String baseURL = "https://od-api.oxforddictionaries.com/api/v2/entries/es/"; 
        String parametrosURL = "?fields=definitions&strictMatch=false";
        OkHttpClient clienteWEB = new OkHttpClient();
        Request request = new Request.Builder()
                .url(baseURL + pBuscar + parametrosURL)
                .get()
                .addHeader("Content-Type","text/json;Charset=UTF-8")
                .addHeader("app_id", "71424ce6")
                .addHeader("app_key", "71602fe536186df4a8d0d11291cd53bb")
                .build();
        com.squareup.okhttp.Response respuesta = clienteWEB.newCall(request).execute();
        /** Solo LOG DE CONSULTA **/
        System.out.println("*** REQUEST A OXFORD ***");
        System.out.println("***" + formatter.format(date) + "***");
        System.out.println("PALABRA:" + pBuscar);
        System.out.println("URL:" + baseURL + pBuscar + parametrosURL);
        /** PARSEO DE JSON con JSONOBJECT y JSONARRAY**/  
        try {
            JSONObject jsonParseado = new JSONObject(respuesta.body().string());
            JSONArray arrDefiniciones = jsonParseado.getJSONArray("results");

            JSONObject auxJson = arrDefiniciones.getJSONObject(0);
            JSONArray auxArrA = auxJson.getJSONArray("lexicalEntries");

            auxJson = auxArrA.getJSONObject(0);
            JSONArray auxArrB = auxJson.getJSONArray("entries");

            auxJson = auxArrB.getJSONObject(0);
            JSONArray auxArrC = auxJson.getJSONArray("senses");

            JSONObject jsonSalida = new JSONObject();
            
            /** PARSEO DE RESULTADO **/
            String auxMsg = "";

            for (int i = 0; i < auxArrC.length(); i++) {
                auxMsg = auxArrC.getJSONObject(i).get("definitions").toString().substring(2);
                auxMsg = auxMsg.substring(0, auxMsg.length() - 2);
                jsonSalida.put("" + i, auxMsg);
            }
            /** Solo LOG DE RESPUESTA VALIDA **/
            System.out.println("*** JSON VALIDO ***");
            System.out.println("***" + formatter.format(date) + "***");
            System.out.println(jsonSalida.toString());
            return jsonSalida; 
        } catch(Exception e) {
            JSONObject jsonSalida = new JSONObject();
            jsonSalida.put("0","*** ERROR ***: Palabra No Existe, intente con una nueva.");
            /** Solo LOG DE RESPUESTA INVALIDO **/
            System.out.println("*** JSON INVALIDO ***");
            System.out.println("***" + formatter.format(date) + "***");
            System.out.println(jsonSalida.toString());
            return jsonSalida;
        }
    }
}
