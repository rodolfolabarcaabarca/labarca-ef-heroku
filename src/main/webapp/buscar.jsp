<%-- 
    Document   : buscar
    Created on : 07-05-2020, 17:23:58
    Author     : rlabarca
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="bootstrap.min.css" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    </head>
    <body> 
        <div class="container">
        <form action="controller" method="POST">
            <table class="table table-hover">
                <tr>
                    <td class="text-center"><h6>Ingrese Palabra a buscar</h6></td>
                </tr>
                <tr>    
                    <td class="text-center"><p>* Acepta Tildes y �, por ej: �rbol, p�gina, �andu, ma�ana *</p></td>
                </tr>
                <tr>
                    <td class="text-center"><input type="text" name="palabra" value="" size="20" maxlength="20" required/></td>
                </tr>
                <tr>
                    <td class="text-center">
                        <input type="text" hidden="true" name="accion" value="search" />
                        <input type="submit" class="btn btn-primary" value="Buscar" />
                        <input type="reset" class="btn btn-info" value="Limpiar" />
                    </td>
                </tr>
            </table>     
        </form>
        </div>
    </body>
</html>