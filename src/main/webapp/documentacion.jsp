<%-- 
    Document   : documentacion
    Created on : 07-05-2020, 20:01:51
    Author     : rlabarca
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <title>API REST : U3.EV3-Rodolfo Labarca</title>
    </head>
    <body>
        <div class="container">
            <h2>Rodolfo Labarca - Examen Final - Taller de Aplicaciones Empresariales</h2>
            <h3>Documentación:</h3>
            <table class="table table-bordered">
                <thead>
                    <tr class="info">
                        <th>Metodo</th>
                        <th>URL</th>
                        <th>Requisitos</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Buscar Palabra API LOCAL (GET)</th>
                        <th><a href="/api/palabra/{PALABRA}">/api/palabra/{PALABRA}</a></th>
                        <th>Invoca el Metodo de buscar Palabra de la API Remota, y Guarda los datos en caso de ser exitosa en la BBDD, en caso de una busqueda repetida actualiza solo la fecha de la busqueda.</th>
                    </tr>
                    <tr>
                        <th>Buscar Palabra API OXFORD DICTIONARY (GET)</th>
                        <th><a href="/api/ox/{PALABRA}">/api/ox/{PALABRA}</a></th>
                        <th>Invoca el metodo de buscar Palabnra en la API DE OXFORD DICTIONARY, ESTE DEVUELVE UN OBJECTO DEL TIPO JSONObject, por esto no es visible en clientes SOAP/REST ** SOLO ES VISIBLE POR LA APLICACION **</a></th>
                    </tr>
                    
                </tbody>
            </table>
        </div>
    </body>
</html>