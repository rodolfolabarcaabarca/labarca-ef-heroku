<%-- 
    Document   : respuesta
    Created on : 07-05-2020, 17:24:08
    Author     : rlabarca
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="bootstrap.min.css" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    </head>
    <body>
        <% 
            String mensaje = (String) request.getAttribute("mensajeSalida");
        %>
        <DIV class="container">
            <table class="table">
                <tr>
                    <td class="text-left"><h5><ol><%=mensaje%></ol></h5></td>
                </tr>
                <tr>
                    <td class="text-center"><a class="btn btn-primary" align="center" href="index.jsp" target="_top">Volver</a></td>

                </tr>
            </table>
        </DIV>
        
        
    </body>
</html>