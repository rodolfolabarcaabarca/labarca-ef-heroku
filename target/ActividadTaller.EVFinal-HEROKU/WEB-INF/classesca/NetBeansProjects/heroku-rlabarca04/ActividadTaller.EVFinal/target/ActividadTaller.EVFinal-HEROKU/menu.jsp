<%-- 
    Document   : menu
    Created on : 07-05-2020, 17:23:51
    Author     : rlabarca
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="bootstrap.min.css" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <h6 class="text-center">Rodolfo Labarca - Examen Final - Taller de Aplicaciones Empresariales</h6>
            <h2 class="text-center">* Busqueda de Palabras *</h2>
            <h4 class="text-center">Seleccione una opcion:</h4>            
            <table class="table table-bordered">
                <tr class="text-center">
                    <td class="text-center">
                        <a href="buscar.jsp" target="principal" class="btn btn-success btn-sm">Buscar Palabra</a>
                        <a href="historico.jsp" target="principal" class="btn btn-info btn-sm">Historial de Busqueda</a>
                        <a href="documentacion.jsp" target="principal" class="btn btn-info btn-sm">Documentacion API</a>
                    </td>
                </tr>
            </table>
            
        </div>
    </body>
</html>

